import { browser } from '$app/env';

/**
 * Create a key for the preference in localStorage
 */
export const key = (label) => 'rscards:' + label;

/**
 * Store a value for a preference
 */
export const set = (key, value) => {
	if (browser) {
		return localStorage.setItem(key, value);
	}
}

/**
 * Get a value for a preference
 */
export const get = (key) => {
	if (browser) {
		return localStorage.getItem(key);
	}
}

/**
 * Remove a value for a preference
 */
export const remove = (key) => {
	if (browser) {
		localStorage.removeItem(key);
	}
}
