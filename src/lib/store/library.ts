/**
 * Svelte stores for storing card decks and cards.
 *
 * Decks and cards will be stored in RemoteStorage
 * and made available to the app through these stores
 */
import { browser } from '$app/env';
import { page } from '$app/stores';
//import { navigation } from '$app/navigation';
import { Flashcards } from 'remotestorage-module-flashcards';
import { writable, derived, get } from 'svelte/store';
import RemoteStorage from 'remotestoragejs';
import * as preference from '$lib/preference';
import { anyIn, allIn } from '$lib/arrays';
import { getById } from '$lib/items';

const folderPref = preference.key('folder');

let folder = preference.get(folderPref)
if (!folder) {
	folder = 'flashcards';
	preference.set(folderPref, folder);
}
let remoteStorage = null;
let decksFetched = false;

export function initialiseRemoteStorage() {
	if (browser) {
		if (remoteStorage) {
			// TODO
      return;
		}

    // Hack to be able to store flashcards whereever
    Flashcards.name = folder;

		remoteStorage = new RemoteStorage({
			modules: [ Flashcards ]
		});

		remoteStorage.access.claim(folder, 'rw');
		remoteStorage.caching.enable('/' + folder + '/', 'ALL');

    import('remotestorage-widget').then((module) => {
      const widget = new module.default(remoteStorage);
      widget.attach();
    });

		// Load decks
		remoteStorage[folder].getAllDecks().then((storedDecks)=> {
			decks.set(storedDecks);
		});
	}
}

initialiseRemoteStorage();

/**
 * Card decks in the RemoteStorage
 */
export const decks = writable(null);

/**
 * A map containing the cards of each deck
 */
export const cards = writable(null);

export const getDecks = (deckIds: string[] | null) => {
	if (!remoteStorage) {
		return null;
	}

	let promise;
	if (decksFetched) {
		promise = Promise.resolve(get(decks));
	} else {
		if (deckIds === null) {
			promise = remoteStorage[folder].getAllDecks().then(($decks) => {
				decks.set($decks);
				decksFetched = true;
				return $decks;
			});
		} else {
			const $decks = get(decks);
			const array = [];
			for (let i = 0; i < deckIds.length; i++) {
				const $deck = $decks && getById($decks, deckIds[i]);
				if ($deck) {
					array.push($deck);
				} else {
					array.push(remoteStorage[folder].getDeck(deckIds[i]));
				}
			}
			promise = Promise.all(array);
		}
	}

	return promise.then(($decks) => {
		if (!decksFetched && deckIds) {
			return $decks;
		}

		if (deckIds === null) {
			return [ ...$decks ];
		}

		const filteredDecks = [];

		for (let i = 0; i < $decks.length; i++) {
			if (deckIds.indexOf($decks[i]['@id']) !== -1) {
				filteredDecks.push($decks[i]);
			}
		}

		return filteredDecks;
	});
};

export const getCards = async (deckIds: string[] | null) => {
	if (!remoteStorage) {
		return null;
	}

	// Ensure have decks first
	if (!decksFetched) {
		await getDecks(deckIds);
	}

	const $decks = get(decks);

	// Get the current visible decks if given null for deckIds
	if (deckIds === null) {
		const currentVisibleDecks = get(visibleDecks);

		// Get ids from decks
		deckIds = [];

		if (currentVisibleDecks?.length) {
			for (let i = 0; i < currentVisibleDecks.length; i++) {
				deckIds.push(currentVisibleDecks[i]['@id']);
			}
		}
	}

	if (!deckIds?.length) {
		return null;
	}

	if (get(cards) === null) {
		cards.set({});
	}

	const promises = [];
	// Only update cards once all cards have been downloaded
	const newValue = {};

	// Ensure have cards for each deck
	for (let i = 0; i < deckIds.length; i++) {
		const id = deckIds[i];
		promises.push(remoteStorage[folder].getDeckCards(id).then((deckCards) => {
			let $deck = null;
			if ($decks?.length) {
				for (let i = 0; i < $decks.length; i++) {
					if ($decks[i]['@id'] === id) {
						$deck = $decks[i];
						break;
					}
				}
			}
			// Add deck details to card
			for (let i = 0; i < deckCards.length; i++) {
				deckCards[i] = {
					...deckCards[i],
					deckId: id,
					deck: $deck
				};
			}
			newValue[id] = deckCards;
		}));
		newValue[id] = null;
	}

	return await Promise.all(promises).then(() => {
		cards.update(v => Object.assign(v, newValue));
	});
};

/**
 * Derived store containing a list of all the
 * tags used in the decks
 */
export const tags = derived(decks, $decks => $decks.reduce((tags, deck) => {
	if (!deck.tags) {
		return tags;
	}

	return deck.tags.reduce((tags, tag) => {
		if (tags.indexOf(tag) === -1) {
			tags.push(tag);
		}

		return tags;
	}, tags);
}));

export const selectedTags = derived(page,
		($page) => $page.url.searchParams.getAll('tag') || []);

export const tagsOred = derived(page,
		($page) => Boolean($page.url.searchParams.get('or')));

/**
 * Decks visible based on the tags in the query string
 */
export const visibleDecks = derived([decks, selectedTags, tagsOred], ([$decks, $selectedTags, $tagsOred]) => {
	if (!$decks || !$selectedTags.length) {
		return $decks;
	}

	return $decks.filter((deck) => {
		if (!deck.tags?.length) {
			return false;
		}

		return $tagsOred ? anyIn(deck.tags, $selectedTags) : allIn(deck.tags, $selectedTags);
	});
});

const checkRemoteStorage = () => {
	if (!remoteStorage) {
		throw new Error('RemoteStorage not initialised');
	}
};

export const storeDeck = (deck, isPublic: boolean = false) => {
	checkRemoteStorage();

	return remoteStorage[folder].storeDeck(deck, isPublic, true);
}

/**
 */
export const toggleTag = (tag: string) => {
}
