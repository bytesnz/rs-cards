export const toggleTagQuery = (tag: string, query: URLSearchParams, tags: string[]) => {
	query = new URLSearchParams(query);
	const index = tags.indexOf(tag);
	if (index === -1) {
		query.append('tag', tag);
	} else {
		tags = [ ...tags ];
		tags.splice(index, 1);
		if (tags.length) {
			query.set('tag', tags);
		} else {
			query.delete('tag');
		}
	}

	const string = query.toString();
	if (string) {
		return '?' + string;
	}

	return string;
};

export const queryString = (query: URLSearchParams) => {
	const string = query.toString();
	if (string) {
		return '?' + string;
	}

	return string;
};
