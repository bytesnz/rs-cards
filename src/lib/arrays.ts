const allIn = (array: any[], values: any[]) => {
	if (!array.length || !values.length || values.length > array.length) {
		return false;
	}

	return typeof values.find(
		(value) => array.indexOf(value) === -1
	) === 'undefined';
};

const anyIn = (array: any[], values: any[]) => {
	if (!array.length || !values.length) {
		return false;
	}

	return typeof values.find(
		(value) => array.indexOf(value) !== -1
	) !== 'undefined';
};

export { allIn, anyIn };
