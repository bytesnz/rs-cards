/**
 * Parse markdown into an array of deck objects
 *
 * @param markdown Markdown to parse into decks and cards
 *
 */
const parse = (markdown: string) => {
	markdown = markdown.replace(/[\r\n]+/g, '\n').trim();

	if (!markdown) {
		return [];
	}

	// Split on the h1 headings
	const decks = markdown.split(/^#\s+/mg);

	// First "deck" should be empty
	decks.shift();

	for (let i = 0; i < decks.length; i++) {
		// Split the deck into cards
		const cards = decks[i].split(/^={3,}$/mg);

		// Last should be empty
		if (!cards[cards.length - 1].trim()) {
			cards.pop();
		}

		// First "card" should be the title,description and tags
		let parts = cards.shift().split('\n');

		decks[i] = {
			name: parts.shift()
		};

		// Handle description/tags
		for (let j = 0; j < parts.length; j++) {
			parts[j] = parts[j].trim();
			if (parts[j].match(/^#[a-zA-Z0-9]/)) {
				if (typeof decks[i].tags === 'undefined') {
					decks[i].tags = [parts[j].slice(1)];
				} else {
					decks[i].tags.push(parts[j].slice(1));
				}
			} else {
				if (decks[i].description) {
					decks[i].description += '\n' + parts[j];
				} else {
					decks[i].description = parts[j];
				}
			}
		}

		// Clean description
		if (typeof decks[i].description !== 'undefined') {
			if (decks[i].description) {
				decks[i].description = decks[i].description.trim();
			}

			if (!decks[i].description) {
				delete decks[i].description;
			}
		}

		// Parse each card
		for (let j = 0; j < cards.length; j++) {
			parts = cards[j].split(/^-{3,}$/mg);

			if (parts.length < 2) {
				// TODO error
			}

			cards[j] = {
				front: {
					content: [
						{
							encodingType: 'text/plain',
							content: parts[0].trim()
						}
					]
				},
				back: {
					content: [
						{
							encondingType: 'text/plain',
							content: parts[1].trim()
						}
					]
				}
			};

			// Add hint from third section
			if (parts.length >= 3 && parts[2].trim()) {
				cards[j].hint = [
					{
						encodingType: 'text/plain',
						content: parts[2].trim()
					}
				];
			}

			// Add notes from fourth section
			if (parts.length >= 4 && parts[3].trim()) {
				cards[j].notes = {
					content: [
						{
							encodingType: 'text/plain',
							content: parts[3].trim()
						}
					]
				};
			}
		}

		decks[i].cards = cards;
	}

	return decks;
};

export { parse };
