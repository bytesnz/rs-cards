interface Item {
	'@id': string | number;
	[key: string]: any;
}

/**
 * Find the first index of the given item in the given array of items
 *
 * @param array Array of items to search
 * @param item Item to search for
 *
 * @returns Index of the item in the array, or -1 if the item is not in
 *   the array
 */
export const find = (array: Item[], item: Item) => {
	for (let i = 0; i < array.length; i++) {
		if (array[i]['@id'] === item['@id']) {
			return i;
		}
	}

	return -1;
}

/**
 * Get the item with the given id from an array of items
 *
 * @param array Array of items to search
 * @param id ID of item to get
 *
 * @returns Item if exists or undefined
 */
export const getById = (array: Item[], id: string | null) => {
	for (let i = 0; i < array.length; i++) {
		if (array[i]['@id'] === id) {
			return array[i];
		}
	}
}
