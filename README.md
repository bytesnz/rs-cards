rs-cards
========

[RemoteStorage][] and [SvelteKit][] flashcards

## Under heavy development

This app is currently under heavy development. Check out
[the merge request](https://gitlab.com/bytesnz/rs-cards/-/merge_requests/1) to
track progress.


## Developing

If you want to develop this project, you can:
* Clone the repository](https://gitlab.com/bytesnz/rs-cards)
* install the dependencies with `npm install` (or `pnpm install` or `yarn`)
* start a development server:

  ```bash
  npm run dev

  # or start the server and open the app in a new browser tab
  npm run dev -- --open
  ```

## Building

The project can be built (using the node adapter) by running
```bash
npm run build
```

> You can preview the built app with `npm run preview`, regardless of whether you installed an adapter. This should _not_ be used to serve your app in production.

[RemoteStorage}; https://remotestorage.io
[SvelteKit]: https;//kit.svelte.dev
